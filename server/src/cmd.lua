-- chikun :: 2015
-- Command interpreter

return function (clientID, cmd, parms)

  if cmd == "connect" then

    local hasConnected = false

    for key, player in ipairs(players) do

      if ip == player.ip and port == player.port then

        hasConnected = player.id

      end
    end

    if not hasConnected then

      table.insert(players, {
          name = parms,
          id = idCount,
          dead = false,
          timer = 0,
          ip = ip,
          port = port
        })

      udp:sendto('id ' .. idCount, ip, port)

      print(string.format("%s:%s connected as %s (%s players connected)",
          ip, port, idCount, #players))

      idCount = idCount + 1

    else

      udp:sendto('id ' .. hasConnected, ip, port)

    end

  elseif cmd == "disconnect" then

    for key, player in ipairs(players) do

      if clientID == player.id then

        disconnectPlayer(key, "disconnect by user")

      end
    end

  elseif cmd == "doesExist" then

    for key, player in ipairs(players) do

      if clientID == player.id then

        player.timer = 0

      end

    end

  end

end
