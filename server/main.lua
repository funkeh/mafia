-- chikun :: 2015
-- Server program for MAFIA in SPACE
-- Requires:
-- * Lua 5.1
-- * LuaSocket

-- Load the LuaSocket library, required for networking
local socket = require("socket")
local interpretCommand = require("src/cmd")

-- Create our server at port 26969 on this machine
udp = socket.udp()
udp:settimeout(0)
udp:setsockname('*', 26969)

-- Incrementing counter of next connected player's ID
idCount = 1

-- Time at which last update happened
local lastTime = 0

-- Keeps track of players on the server
players = { }


-- Will kick a player and report for a reasons
function disconnectPlayer(key, reason)

  -- Report the reason to the server's console
  print(string.format("Player %s disconnected (%s)", players[key].id, reason))

  -- Actually remove player from list
  table.remove(players, key)

end


-- Game will continue looping while running is true
local running = true
while running do

  -- Calculate delta time and update lastTime
  dt = os.clock() - lastTime
  lastTime = os.clock()

  -- Repeat until all data processed
  repeat

    -- Receive a packet of data
    data, ip, port = udp:receivefrom()

    -- If that packet contains data, process it
    if data then

      -- Separate packet of data
      local clientID, cmd, parms = data:match("^(%S*) (%S*) (.*)")
      clientID = tonumber(clientID)

      -- Interpret data
      interpretCommand(clientID, cmd, parms)

    end

  -- End loop if no data
  until not data


  for key, player in ipairs(players) do

    -- Increase timer
    player.timer = player.timer + dt

    -- If idle timer breaches 10 seconds
    if player.timer >= 10 then

      -- Kick for idling
      disconnectPlayer(key, 'idle for too long')

    end
  end
end
