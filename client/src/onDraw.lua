return function()

  lg.setColour(255, 255, 255)

  for key, star in ipairs(starfield) do

    lg.rectangle('fill', star.x * game.width, star.y * game.height, 1, 1)

  end

  centre = {
    x = 600,
    y = 675 / 2
  }

  for key, player in ipairs(players) do

    drawInfoBox(key, player)

  end

  local chatbox = {
    x = 16,
    y = 416,
    w = 368,
    h = 243
  }

  drawUIBox(16, 16, 200, 24)
  lg.print(player.name, 20, 20)

  drawUIBox(16, 56, 128, 138)
  for key, role in ipairs(roleList) do
    lg.setColor(role.colour)
    lg.print(role.name, 20, 60 + (key - 1) * 20)
  end

  drawUIBox(chatbox.x, chatbox.y, chatbox.w, chatbox.h)
  lg.setScissor(chatbox.x, chatbox.y, chatbox.w, chatbox.h)
  lg.printf(string.format("Player: %s", chatMessage),
    chatbox.x + 4, chatbox.y + 4, chatbox.w - 8)
  lg.setScissor()

end
