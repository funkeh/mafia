-- chikun :: 2015
-- To be run with LOVE

-- Load the LuaSocket libary, required for networking
local socket = require "socket"

require "src/bindings"  -- Shorthand bindings
require "src/ui"        -- Extra functions for UI

-- Core functions
love.load =      require "src/onLoad"
love.update =    require "src/onUpdate"
love.draw =      require "src/onDraw"
love.textinput = require "src/onTextInput"
love.quit =      require "src/onQuit"
