-- chikun :: 2015
-- Performed on loading of game

return function()

  -- Unique ID given to client by the server to identify itself
  -- false if not received ID yet
  id = false

  -- Our player object
  player = {
    name = "Super Guy"
  }

  -- Controls how regularly the client tells the server it exists
  sendTimer = 1

  udp = socket.udp()

  udp:settimeout(0)

  udp:setpeername('mafiainspace.no-ip.org', 26969)

  players = {
    { name = "Marcus",
      role = "mafia",
      dead = false
    },
    { name = "Thomas",
      role = "mafia",
      dead = false
    },
    { name = "Tex",
      role = "sheriff",
      dead = false
    },
    { name = "James",
      role = "townspeople",
      dead = false
    },
    { name = "Roger",
      role = "townspeople",
      dead = false
    },
    { name = "Pablo",
      role = "doctor",
      dead = false
    }
  }

  roles = {
    mafia = {
      name = "Mafia Member",
      colour = { 200, 20, 20 }
    },
    sheriff = {
      name = "Sheriff",
      colour = { 200, 20, 200 }
    },
    townspeople = {
      name = "Townsperson",
      colour = { 40, 120, 220 }
    },
    doctor = {
      name = "Doctor",
      colour = { 200, 200, 20 }
    },
  }

  roleList = { }

  for key, player in ipairs(players) do

    table.insert(roleList, roles[player.role])

  end

  table.sort(roleList, function(a,b)
      return a.name < b.name
    end)

  game = {
    width = lw.getWidth(),
    height = lw.getHeight()
  }

  chatlog = { }

  chatMessage = ""


  starfield = { }

  for i = 1, 1024 do

    local speed = math.random() / 16

    table.insert(starfield, {
        x = math.random(),
        y = math.random(),
        hSpeed = 0.03 + speed * 0.04,
        vSpeed = 0.02 + speed * 0.02
      })

  end
end
