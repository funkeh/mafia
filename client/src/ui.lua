function drawInfoBox(key, player)

  -- Direction from origin at which box will be drawn
  local direction = math.rad((key - 1) / #players * 360)

  local box = {
    x = (centre.x + math.cos(direction) * 120) - 60,
    y = (centre.y + math.sin(direction) * 120) - 20,
    w = 80,
    h = 40
  }

  lg.setColour(200, 50, 80)
  lg.rectangle('fill', box.x, box.y, box.w, box.h)
  lg.setColour(240, 120, 160)
  lg.rectangle('line', box.x+1, box.y+1, box.w-1, box.h-1)

  lg.setColour(255, 255, 255)
  lg.print(player.name .. "\n" .. player.role, box.x + 2, box.y + 2)

end

function drawUIBox(x, y, w, h)

  lg.setColour(51, 0, 26)
  lg.rectangle('fill', x, y, w, h)

  lg.setColour(92, 51, 72)
  lg.rectangle('line', x + 1, y + 1, w - 1, h - 1)

  lg.setColour(255, 255, 255)

end
