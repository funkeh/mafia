-- chikun :: 2015
-- Binds core libraries to short terms and sets functions

lg = love.graphics
lg.setColour = lg.setColor

lk = love.keyboard
lk.setKeyRepeat(true)

lw = love.window
