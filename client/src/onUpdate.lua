-- chikun ::2015
-- Performed on game update

return function (dt)

  -- Variables used for interpreting incoming data
  local data, msg, ent, cmd, parms

  -- Restrict delta time
  dt = math.min(dt, 1/15)

  sendTimer = sendTimer + dt

  if sendTimer >= 1/4 then
    if id then
      udp:send(id .. " doesExist ...")
      sendTimer = sendTimer - 1/4
    else
      udp:send("... connect Dickbag")
      sendTimer = sendTimer - 1
    end
  end

  for key, star in ipairs(starfield) do

    star.x = (star.x + star.hSpeed * dt) % 1
    star.y = (star.y + star.vSpeed * dt) % 1

  end

  repeat

    data, msg = udp:receive()

    if data then

      cmd, parms = data:match("^(%S*) (.*)")

      if cmd == "id" then

        id = tonumber(parms)

      elseif cmd == "setName" then

        player.name = parms

      end
    end

  until not data

end
